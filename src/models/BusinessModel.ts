import { FirestoreClient } from './FirestoreClient.js';

export class BusinessModel extends FirestoreClient {
    static ID = 'id';
    static NAME = 'name';
    static COUNTRY = 'country';
    static STATE = 'state';
    static CITY = 'city';
    static PINCODE = 'pincode';
    static CREATED_BY = 'created_by';
    static IDENTIFIER = 'identifier';
        
    constructor() {
       super('businesses'); 
    }
}