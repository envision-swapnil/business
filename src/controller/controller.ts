import { pick } from 'lodash';
import Axios from 'axios';
import { BusinessModel } from '../models/BusinessModel.js';
import { saveSchema, updateSchema } from '../schema/schema.js';
import {
    CAN_READ_BUSINESS,
    CAN_CREATE_BUSINESS,
    CAN_UPDATE_BUSINESS,
    CAN_DELETE_BUSINESS
} from '../constants/constants';

const Business = new BusinessModel();

const AUTH_URL = 'https://59phci4y38.execute-api.ap-south-1.amazonaws.com/dev';

const response = (callback, statusCode, body) => {
    return callback(null, {
        statusCode: statusCode || 200,
        body: JSON.stringify(body)
    });
}

export const getBusinessController = async (event, callback) => {
    const { permissions } = event;
    // if (!permissions[CAN_READ_BUSINESS]) return response(callback, 401, 'Unauthorized.');

    const { business_id = '' } = event.pathParameters || {}

    if (!business_id) {
        const { business_ids = [] } = event;

        if(!business_ids.length) return response(callback, 200, {});

        const snapshot = await Business.collectionRef.where(BusinessModel.ID, 'in', business_ids).get();
        if (snapshot.empty) return response(callback, 200, {});

        const businesses: object = {};
        snapshot.docs.forEach(doc => businesses[doc.id] = doc.data())

        return response(callback, 200, businesses);
    }

    const business = await Business.getDoc(business_id);

    if (!business.exists) return response(callback, 400, 'Business does not exist.');

    response(callback, 200, business.data());
}

const getNewToken = async (id, headers) => {
    const oldToken = headers['x-auth-token'];

    const { data } = await Axios({
        method: 'get',
        url: `${AUTH_URL}/refresh-token/${id}`,
        headers: {
            'x-auth-token': oldToken
        }
    });

    return data.token;
}

export const saveBusinessController = async (event, callback) => {
    const { permissions } = event;
    // if (!permissions[CAN_CREATE_BUSINESS]) return response(callback, 401, 'Unauthorized.');

    const { identifier, user_id } = event;
    const { business_details } = event.body;

    business_details['identifier'] = identifier;
    business_details['created_by'] = user_id;

    const { error } = saveSchema(business_details);
    if (error) return response(callback, 400, error.details[0].message);

    const { id } = Business.collectionRef.doc();

    business_details['id'] = id;
    await Business.set(id, business_details);

    const token = await getNewToken(id, event.headers);
    response(callback, 200, { token, business: business_details });
}

export const deleteBusinessController = async (event, callback) => {
    const { permissions } = event;
    if (!permissions[CAN_DELETE_BUSINESS]) return response(callback, 401, 'Unauthorized.');

    const { business_id } = event.pathParameters || {};

    const { status } = await Axios({
        method: 'put',
        url: `${AUTH_URL}/`,
        data: {
            type: 'REMOVE_BUSINESS_ID',
            business_id
        },
        headers: {
            'x-auth-token': event.headers['x-auth-token']
        }
    });

    if (status === 200) await Business.delete(business_id);
    else response(callback, 500, 'Something went wrong.');
}

export const updateBusinessController = async (event, callback) => {
    const { permissions } = event;
    if (!permissions[CAN_UPDATE_BUSINESS]) return response(callback, 401, 'Unauthorized.');

    const { business_id } = event.pathParameters || {};
    const { business_details = {} } = event.body;

    const { error } = updateSchema(business_details);
    if (error) return response(callback, 400, error.details[0].message);

    await Business.update(business_id, business_details);
}