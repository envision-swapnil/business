import {
    getBusinessController,
    saveBusinessController,
    deleteBusinessController,
    updateBusinessController
} from '../controller/controller.js';
import { applyMiddleware, verifyJwtToken, verifyBusiness } from 'middlewares-nodejs';

export const getBusiness = (event, context, callback) => {
    const middlewares = [verifyJwtToken, verifyBusiness];
    applyMiddleware(event, callback, middlewares, getBusinessController);
}

export const saveBusiness  = (event, context, callback) => {
    const middlewares = [verifyJwtToken];
    applyMiddleware(event, callback, middlewares, saveBusinessController);
}

export const deleteBusiness = (event, context, callback) => {
    const middlewares = [verifyJwtToken, verifyBusiness];
    applyMiddleware(event, callback, middlewares, deleteBusinessController);
}

export const updateBusiness = (event, context, callback) => {
    const middlewares = [verifyJwtToken, verifyBusiness];
    applyMiddleware(event, callback, middlewares, updateBusinessController);
}

export const healthCheck  = (event, context, callback) => {
    const response = {
        statusCode: 200,
        body: JSON.stringify(`You're being served from ap-south-1 Mumbai region. (Business API)`)
    };

    callback(null, response);
}