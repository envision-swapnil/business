import * as Joi from '@hapi/joi';
import { BusinessModel } from '../models/BusinessModel.js';

const getFields = () => {
    return {
        [BusinessModel.NAME]: Joi.string().pattern(/^[a-zA-Z ]+$/),
        [BusinessModel.COUNTRY]: Joi.string(),
        [BusinessModel.STATE]: Joi.string(),
        [BusinessModel.CITY]: Joi.string(),
        [BusinessModel.PINCODE]: Joi.string().min(6).max(6),
        [BusinessModel.CREATED_BY]: Joi.string(),
        [BusinessModel.IDENTIFIER]: Joi.string()
    };
};

export const saveSchema = (data) => {
    const fields = getFields();
    const schema = Joi.object({
        [BusinessModel.NAME]: fields[BusinessModel.NAME].required(),
        [BusinessModel.COUNTRY]: fields[BusinessModel.COUNTRY].required(),
        [BusinessModel.STATE]: fields[BusinessModel.STATE].required(),
        [BusinessModel.CITY]: fields[BusinessModel.CITY].required(),
        [BusinessModel.PINCODE]: fields[BusinessModel.PINCODE].required(),
        [BusinessModel.CREATED_BY]: fields[BusinessModel.CREATED_BY].required(),
        [BusinessModel.IDENTIFIER]: fields[BusinessModel.IDENTIFIER].required()
    });

    return schema.validate(data);
}

export const updateSchema = (data) => {
    const fields = getFields();
    const schema = Joi.object({
        [BusinessModel.NAME]: fields[BusinessModel.NAME],
        [BusinessModel.COUNTRY]: fields[BusinessModel.COUNTRY],
        [BusinessModel.STATE]: fields[BusinessModel.STATE],
        [BusinessModel.CITY]: fields[BusinessModel.CITY],
        [BusinessModel.PINCODE]: fields[BusinessModel.PINCODE],
    });

    return schema.validate(data);
}
